#FROM nginx:alpine
#COPY ./public /usr/share/nginx/html
# Base Image. 
FROM ubuntu:16.04 

# Update the Repo. 
RUN apt-get update
RUN apt-get install wget unzip -y
RUN wget https://releases.hashicorp.com/terraform/0.12.2/terraform_0.12.2_linux_amd64.zip
RUN unzip ./terraform_0.12.2_linux_amd64.zip
RUN mv terraform /usr/local/bin
RUN terraform -version

